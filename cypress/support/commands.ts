/// <reference types="cypress" />
import * as cypressQueries from './queries-4-cypress'
import * as xpathQueries from './queries-4-xpath'

const COMMAND_DELAY = 1250
const COMMANDS = [
  'clear',
  'click',
  'contains',
  'reload',
  'trigger',
  'type',
  'visit'
]

declare global {
  namespace Cypress {
    interface Chainable {
      login: () => Chainable<JQuery<HTMLElement>>
      createNewInvoice: () => Chainable<Element>
      getNumberElectronicInvoices: () => Promise<number>
      downloadLatestInvoice: () => Chainable<Element>
    }
  }
}

// Setup delay between each cypress command
COMMANDS.forEach((command) => {
  Cypress.Commands.overwrite(command, (originalFn, ...args) => {
    const origVal = originalFn(...args)
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(origVal)
      }, COMMAND_DELAY)
    })
  })
})

function login(): void {
  cy.get(cypressQueries.labelRecarrega).last().click()
  cy.get(cypressQueries.inputUser).clear().type(Cypress.env('CYPRESS_CMC'))
  cy.get(cypressQueries.inputEmail).clear().type(Cypress.env('CYPRESS_EMAIL'))
  cy.get(cypressQueries.inputPassword)
    .clear()
    .type(Cypress.env('CYPRESS_PASSWORD'))

  cy.get(cypressQueries.login).click()
}

function newInvoiceSetup(): void {
  cy.get(cypressQueries.newInvoiceButton).first().click()
  cy.get(cypressQueries.inputDate).click()
  cy.xpath(xpathQueries.selectDayQuery).click()
  cy.xpath(xpathQueries.confirmButtonQuery).click()
}

function newInvoiceStepOne(): void {
  cy.get(cypressQueries.inputCnpj)
    .clear()
    .type(Cypress.env('CYPRESS_CNPJ_RECEIVER'))

  if (Cypress.env('CYPRESS_REWRITE_DEFAULT_VALUES_FIRST_STEP')) {
    const cityNumber = Cypress.env('CYPRESS_CITY_NUMBER')
    cy.get(cypressQueries.inputSocialName)
      .clear()
      .type(Cypress.env('CYPRESS_SOCIAL_NAME'))

    if (cityNumber) {
      cy.get(cypressQueries.inputCityCode).clear().type(cityNumber)
    }
    cy.get(cypressQueries.inputZipCode)
      .clear()
      .type(Cypress.env('CYPRESS_ZIP_CODE'))

    cy.get(cypressQueries.inputNumber)
      .clear()
      .type(Cypress.env('CYPRESS_STREET_NUMBER'))

    cy.get(cypressQueries.inputComplement)
      .clear()
      .type(Cypress.env('CYPRESS_STREET_COMPLEMENT'))

    cy.get(cypressQueries.inputEmailId)
      .clear()
      .type(Cypress.env('CYPRESS_EMAIL_RECEIVER'))

    cy.get(cypressQueries.inputPhone)
      .clear()
      .type(Cypress.env('CYPRESS_PHONE_RECEIVER'))
  }

  cy.xpath(xpathQueries.nextButtonQuery).first().click()
  cy.xpath(xpathQueries.cfpsDivQuery).click()
  cy.xpath(xpathQueries.selectButtonQuery).click()
}

function newInvoiceStepTwo(): void {
  const serviceDescription = Cypress.env('CYPRESS_SERVICE_DESCRIPTION')
  const additionalInformation = Cypress.env('CYPRESS_ADDITIONAL_INFORMATION')
  const cst =
    '1 - Tributada integralmente e sujeita ao regime do Simples Nacional'

  cy.xpath(xpathQueries.addServiceButtonQuery).click()
  cy.get(cypressQueries.inputCnae)
    .clear()
    .type(Cypress.env('CYPRESS_CNAE_CODE'))

  cy.get(cypressQueries.inputCnae).type('{enter}')
  if (serviceDescription) {
    cy.get(cypressQueries.inputDescription)
      .clear()
      .type(Cypress.env('CYPRESS'))
  }

  cy.get(cypressQueries.inputCst).select(cst)
  cy.get(cypressQueries.textPricePerUnit).click()
  cy.get(cypressQueries.numberPricePerUnit)
    .type(Cypress.env('CYPRESS_PRICE_PER_INVOICE'))

  cy.get(cypressQueries.textAmount).click()
  cy.get(cypressQueries.numberAmount)
    .type(Cypress.env('CYPRESS_INVOICE_AMOUNT'))

  cy.xpath(xpathQueries.addButtonQuery).click()
  if (additionalInformation) {
    cy.get(cypressQueries.additionalInfo).type(additionalInformation)
  }

  cy.xpath(xpathQueries.nextButtonQuery).click()
}

function newInvoiceStepThree(): void {
  const shouldExecuteStepThree = Cypress.env('CYPRESS_ENABLE_STEP_THREE')
  if (shouldExecuteStepThree) {
    cy.get(cypressQueries.transmit).click()
  }
}

function createNewInvoice(): void {
  newInvoiceSetup()
  newInvoiceStepOne()
  newInvoiceStepTwo()
  newInvoiceStepThree()
}

function listInvoices(): void {
  cy.xpath(xpathQueries.submitButtonQuery).click()
}

function getNumberElectronicInvoices(): Promise<number> {
  listInvoices()
  const numberElectronicInvoices =
    Number(cy.xpath(xpathQueries.lastetElectronicInvoiceNumber).invoke('text'))
  return Promise.resolve(numberElectronicInvoices)
}

function downloadLatestInvoice(): void {
  listInvoices()
  cy.xpath(xpathQueries.lastetElectronicInvoiceId)
    // @ts-ignore
    .then(([{ id: electronicInvoiceId }]) => {

    const baseUrl = Cypress.env('CYPRESS_URL_DOWNLOAD_ELECTRONIC_INVOICE')
    const userId = Cypress.env('CYPRESS_CMC')
    const targetUrl = `${baseUrl}/${electronicInvoiceId}/${userId}`
    cy.request(targetUrl).then((res) => {
      const { body } = res
      cy.writeFile('/tmp/notaBase64.txt', body)
    })
  })
}

Cypress.Commands.add('login', login)
Cypress.Commands.add('createNewInvoice', createNewInvoice)
Cypress.Commands.add('getNumberElectronicInvoices', getNumberElectronicInvoices)
Cypress.Commands.add('downloadLatestInvoice', downloadLatestInvoice)
