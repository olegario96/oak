import { cfpsCodeParser, formatDayNumber } from '../../src/helpers'

const day = formatDayNumber(Cypress.env('CYPRESS_DAY'))
const cfpsIndex = cfpsCodeParser(Cypress.env('CYPRESS_CFPS_CODE'))

export const selectDayQuery =
  `//td[@data-handler='selectDay']/a[text()='${day}']`

export const confirmButtonQuery = '//button[text()="Confirmar"]'
export const nextButtonQuery = '//span[text()="Prosseguir"]'
export const cfpsDivQuery =
  `//div[@ng-repeat='cfps in listaCFPS'][${cfpsIndex}]`

export const selectButtonQuery = '//button[text()="Selecionar"]'
export const addServiceButtonQuery = '//span[text()="ADICIONAR SERVIÇO"]'
export const addButtonQuery = '//button[text()="Adicionar"]'
export const submitButtonQuery = '//button[@type="submit"]'
export const lastetElectronicInvoiceNumber =
  '(//tr)[2]/td[@class="ng-binding"][1]'

export const lastetElectronicInvoiceId = '(//tr)[2]/td[1]/div/input'
