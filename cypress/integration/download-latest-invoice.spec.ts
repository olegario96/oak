describe('Download latest electronic invoice', () => {
  it('should download latest electronic invoice', () => {
    cy.visit('/').then(() => {
      cy.login().then(() => {
        cy.downloadLatestInvoice().then(() => {
          cy.readFile('/tmp/notaBase64.txt').then((content) => {
            expect(content).not.to.equal(null)
            expect(content).not.to.equal('')
          })
        })
      })
    })
  })
})
