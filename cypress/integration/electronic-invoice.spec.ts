describe('Electronic invoice', () => {
  it('should generate an electronic invoice', () => {
    cy.visit('/').then(() => {
      cy.login().then(() => {
        cy.getNumberElectronicInvoices().then((numberElectronicInvoices) => {
          cy.createNewInvoice().then(() => {
            cy.visit('/').then(() => {
              cy.getNumberElectronicInvoices().then(finalElectronicInvoices => {
                const expectedEletronicInvoices =
                  Cypress.env('CYPRESS_ENABLE_STEP_THREE')
                    ? numberElectronicInvoices + 1
                    : numberElectronicInvoices

                expect(expectedEletronicInvoices)
                  .to.equal(finalElectronicInvoices)
              })
            })
          })
        })
      })
    })
  })
})
