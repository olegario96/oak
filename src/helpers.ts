const expectedCfpsCodes = [
  9201,
  9205,
  9208,
]

// Indicates values for dropdown
const alternativeCfpsCodes = [
  1, // tslint:disable
  2, // tslint:disable
  3,
]

/**
 * It returns the number of days in a specific month of a year.
 * @param {number} year Year to be checked
 * @param {number} month Month number (i.e. value between 1-12)
 */
function daysInMonth(year: number, month: number): Number {
  return new Date(year, month, 0).getDate()
}

/**
 * It ensure that CFPS code provided by the user is a valid one.
 * If it is valid, it will return the index value from the array
 * used to check if the number is valid. Otherwise, it will throw
 * an `Error` saying that the code is invalid.
 * @param {number} cfpsCode CFPS code provided by the user. Must be
 * one of the following values: 9201, 9205, 9208, 1, 2 or 3.
 */
function cfpsCodeParser(cfpsCode: number): number {
  const indexForExpectedCfpsCode = expectedCfpsCodes.indexOf(cfpsCode)
  if (indexForExpectedCfpsCode >= 0) return indexForExpectedCfpsCode + 1

  const indexForAlternativeCfpsCode = alternativeCfpsCodes.indexOf(cfpsCode)
  if (indexForAlternativeCfpsCode >= 0) return indexForAlternativeCfpsCode + 1

  const errMsg = `Invalid cfps code supplied! Must be one of the following values: \
                  ${expectedCfpsCodes.concat(alternativeCfpsCodes).toString()}`

  throw Error(errMsg.replace(/\s+/g, ' '))
}

/**
 * It receives a `string` which represents a day that can be using or two digits.
 * It will check if the day exists in the currrent month. If so, it will return
 * the day using `Number` type. Otherwise, it will thrown an `Error`.
 * @param {string} day A day that can be using one or two digits.
 */
function formatDayNumber(day: string): number {
  const dayNumber = Number(day)
  const currentDate = new Date()
  const daysInCurrentMonth = daysInMonth(currentDate.getMonth() + 1, currentDate.getFullYear())
  if (dayNumber < 1 || dayNumber > daysInCurrentMonth) throw Error('Invalid day supplied!')
  return dayNumber
}

export {
  alternativeCfpsCodes,
  cfpsCodeParser,
  expectedCfpsCodes,
  formatDayNumber,
}
