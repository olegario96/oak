import { readFileSync } from 'fs'
import { base64Decode } from 'base64topdf'

async function main(): Promise<void> {
  const contentEncoded = readFileSync('/tmp/notaBase64.txt', 'utf-8').toString()
  base64Decode(contentEncoded, '/tmp/nota.pdf')
  // TODO
  // SAVE ELECTRONIC INVOICE ON MY GOOGLE DRIVE
  // SEND NOTE THROUGH ZOHO API
  // SEND SNS Message
}

main().then(_ => ({}))
