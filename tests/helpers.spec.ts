import {
  alternativeCfpsCodes, cfpsCodeParser, expectedCfpsCodes, formatDayNumber,
} from '../src/helpers'

describe('formatDayNumber', () => {
  it('should format a day number with one digit', () => {
    const dayNumber = '1'
    expect(formatDayNumber(dayNumber)).toBe(1)
  })

  it('should thrown exception with invalid day', () => {
    const dayNumber = '0'
    expect(() => formatDayNumber(dayNumber)).toThrow('Invalid day supplied!')
  })
})

describe('cfpsCodeParser', () => {
  it('should return an index for valid every cfps code', () => {
    const cpfsCodes = alternativeCfpsCodes.concat(expectedCfpsCodes)
    const indexes = alternativeCfpsCodes
    cpfsCodes.map((cpfsCode) =>
      expect(indexes).toContain(cfpsCodeParser(cpfsCode))
    )
  })

  it('should thrown exception with invalid cfps code', () => {
    const invalidCode = 7
    const errMsg =
      `Invalid cfps code supplied! Must be one of the following values: \
      ${expectedCfpsCodes.concat(alternativeCfpsCodes).toString()}`

    expect(() => cfpsCodeParser(invalidCode))
      .toThrow(errMsg.replace(/\s+/g, ' '))
  })
})
