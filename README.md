# oak

Project to easily generate electronic invoice for Prefeitura Municipal de Florianópolis using Cypress API.

## Install dependencies

```
$ yarn
```

After install all packages you have to fill the `cypress.env.json` file with the desired values. In the following section, you can check the purpose of each variable.

### `cypress.env.json` variables
In this section you can see every variable availabe on the `cypress.env.json` file and for
what they are designed for:
 - `CYPRESS_CMC`: your CMC code used in NFPS-E system.

 - `CYPRESS_EMAIL`: your email used in NFPS-E system.

 - `CYPRESS_PASSWORD`: your password used in NFPS-E system.

 - `CYPRESS_DAY`: day when the eletronic invoice will be generated. It can use one or two digits.

 - `CYPRESS_CNPJ_RECEIVER`: CNPJ code from the company that will pay for the eletronic invoice.

 - `CYPRESS_REWRITE_DEFAULT_VALUES_FIRST_STEP`: boolean to indicate if you want to rewrite all the fields that are automatically filled by the `CYPRESS_CNPJ_RECEIVER`

 - `CYPRESS_SOCIAL_NAME`: Social name of the company that will pay for the eletronic invoice. This is mandatory in case of `CYPRESS_REWRITE_DEFAULT_VALUES_FIRST_STEP` be set as `true`.

 - `CYPRESS_CITY_NUMBER`: City number code for the company that will pay for the eletronic invoice. This is an optional field.

 - `CYPRESS_ZIP_CODE`: Zip code for the company that will pay for the eletronic invoice. This is a mandatory field in case of `CYPRESS_REWRITE_DEFAULT_VALUES_FIRST_STEP` be set as `true`.

 - `CYPRESS_STREET_NUMBER`: Street number for the company that will pay for the eletronic invoice. This is a mandatory field in case of `CYPRESS_REWRITE_DEFAULT_VALUES_FIRST_STEP` be set as `true`.

 - `CYPRESS_STREET_COMPLEMENT`: Street complement for the company that will pay for the eletronic invoice. This is a mandatory field in case of `CYPRESS_REWRITE_DEFAULT_VALUES_FIRST_STEP` be set as `true`.

 - `CYPRESS_EMAIL_RECEIVER`: Company email that will pay for the eletronic invoice. This is a mandatory field in case of `CYPRESS_REWRITE_DEFAULT_VALUES_FIRST_STEP` be set as `true`.

 - `CYPRESS_PHONE_RECEIVER`: Phone for the company that will pay for the eletronic invoice. This is a mandatory field in case of `CYPRESS_REWRITE_DEFAULT_VALUES_FIRST_STEP` be set as `true`.

 - `CYPRESS_CFPS_CODE`: CFPS code. It must be 9201, 9205, 9208, 1, 2 or 3.

 - `CYPRESS_CNAE_CODE`: CNAE code for your company.

 - `CYPRESS_SERVICE_DESCRIPTION`: Description for the service that your company did it. This is an optional field.

 - `CYPRESS_PRICE_PER_INVOICE`: Amount that you charge the company per serivce. This field is expressed in BRL.

 - `CYPRESS_INVOICE_AMOUNT`: Number of services that your company did it.

 - `CYPRESS_ADDITIONAL_INFORMATION`: Any additional information that you may want to add to the eletronic invoice.

 - `CYPRESS_ENABLE_STEP_THREE`: Boolean to indicate if you really want that oak finishes the eletronic invoice generation process.

### Running the project

```
$ yarn start
```

### Running tests

```
$ yarn test
```

### Running linter

```
$ yarn lint
```

### Generate documentation

```
$ yarn docs
```
